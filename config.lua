
--[[
  Satelito website configuration file.
  <https://codeberg.org/hs0ucy/Satelito/src/branch/master/README.md>
]]

return {
  --[[
    App Mandatory Values
    (If you edit them do it carefully)

    - siteurl
    - language
    - paths.*
    - mimetypes.*
  ]]
  sitemapxml = true,
  siteurl = "https://grgmelies.tld",
  language = "en",

  -- Main paths
  -- Must be relative paths to the config.lua
  paths = {
    content = 'content/', -- where are the source files (markdown, lua)
    templates = 'templates/', -- where are the etlua templates
    public_html = 'public_html/', -- where you export your site
    -- You can add yours here
  },

  -- Accepted mime types of the non-textual content
  mimetypes = {
    'image/svg+xml',
    'image/gif',
    'image/jpeg',
    'image/png',
    'application/pdf',
    -- You can add yours here
  },

  --[[
    Templates specific values
  ]]
  metas = {
    generator = "Satelito",
  },

  author = {
    name = "Georges Méliès",
    uri = "https://grgmelies.tld/#",
    relme = {
      "https://mastodon.sdf.org/@grgmelies",
      "https://pixelfed.social/grgmelies",
      "https://peertube.social/accounts/grgmelies",
      "https://codeberg.org/grgmelies",
    },
  },
  -- You can add yours here
}
