
Hello my name is Georges Méliès
===============================

I was a French illusionist, actor and film director who led many
technical and narrative developments in the earliest days of
cinema.Méliès was well known for the use of special effects,
popularizing such techniques as substitution splices, multiple
exposures, time-lapse photography, dissolves, etc.

![Picture of Georges Méliès en 1890](/426px-George_Melies.jpg)
