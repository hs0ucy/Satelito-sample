
# A Moonlight Serenade

Au clair de la Lune ou Pierrot malheureux, sold in the United States
as A Moonlight Serenade, or the Miser Punished and in Britain as
Pierrot and the Moon, is a 1903 French short silent film by Georges
Méliès. It was sold by Méliès's Star Film Company and is numbered
538–539 in its catalogues.
