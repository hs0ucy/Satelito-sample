# A Trip to the Moon

A Trip to the Moon (French: Le Voyage dans la Lune)[a] is a 1902
French adventure short film directed by Georges Méliès.

![](/filmography/20171125023119!Voyage_dans_la_lune_title_card.png)

Inspired by a wide variety of sources, including Jules Verne's 1865
novel From the Earth to the Moon and its 1870 sequel Around the Moon,
the film follows a group of astronomers who travel to the Moon in a
cannon-propelled capsule, explore the Moon's surface, escape from an
underground group of Selenites (lunar inhabitants), and return to
Earth with a captive Selenite.

It features an ensemble cast of French theatrical performers, led by
Méliès himself in the main role of Professor Barbenfouillis, and is
filmed in the overtly theatrical style for which Méliès became famous.
