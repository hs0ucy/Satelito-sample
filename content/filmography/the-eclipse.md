
# The Eclipse: Courtship of the Sun and Moon

A professor of astronomy gives a lecture instructing on an impending
solar eclipse. The class rushes to an observation tower to witness the
event, which features an anthropomorphic Sun and Moon coming
together. The Moon and the Sun lick their lips in anticipation as the
eclipse arrives, culminating in a romantic encounter between the two
celestial bodies. Various heavenly bodies, including planets and
moons, hang in the night sky; a meteor shower is depicted using the
ghostly figures of girls. The professor of astronomy, shocked by all
he has witnessed, topples from the observation tower. Fortunately, he
lands in a rain barrel, and is revived by his students.

![](/filmography/the-eclipse.jpg)
